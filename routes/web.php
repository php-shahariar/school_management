<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['verify' => true, 'register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/{anypath}', 'HomeController@index')->where('any', '.*');


Route::get('/class-list', 'School\DepartmentController@index')->where('any', '.*');
Route::post('/add/new-class', 'School\DepartmentController@store');
Route::get('/class-edit/{department}', 'School\DepartmentController@edit');
Route::post('/class-update/{department}', 'School\DepartmentController@update');
Route::delete('/class-delete/{department}', 'School\DepartmentController@destroy');
Route::get('/xhr/show/all/class', 'School\DepartmentController@classes');

// Subject router
Route::get('/subjects', 'School\SubjectController@index')->where('any', '.*');
Route::get('/create/subject', 'School\SubjectController@create');
Route::post('/xhr/add/new-subject', 'School\SubjectController@store');
Route::get('/xhr/show/all/subjects', 'School\SubjectController@subjects');
Route::delete('/subject-delete/{subject}', 'School\SubjectController@destroy');
Route::get('/subject-edit/{subject}', 'School\SubjectController@edit');
Route::post('/subject-update/{subject}', 'School\SubjectController@update');

// Library router
Route::get('/book-list', 'Library\LibraryController@index')->where('any', '.*');
Route::get('/create/book', 'Library\LibraryController@create');
Route::get('/book-edit/{library}', 'Library\LibraryController@edit');
Route::post('/book-update/{library}', 'Library\LibraryController@update');
Route::get('/xhr/show/all/subject', 'Library\LibraryController@subjects');
Route::post('/add/new/subject', 'Library\LibraryController@store');
Route::get('/show/all/book', 'Library\LibraryController@books');
Route::get('/xhr/show/all/class', 'Library\LibraryController@classes');
Route::post('/book/{library}/active', 'Library\LibraryController@active');
Route::post('/book/{library}/inactive', 'Library\LibraryController@inactive');
Route::delete('/book-delete/{library}', 'Library\LibraryController@destroy');
Route::get('/trash/book/list', 'Library\LibraryController@trashList');
Route::get('/trash/list', 'Library\LibraryController@trash');
Route::get('/book-restore/{library}', 'Library\LibraryController@restore');
Route::get('/book-delete/{library}', 'Library\LibraryController@libraryDelete');

// BookCollect
Route::get('/collect-book', 'Library\LibraryController@showBookCollectForm')->where('any', '.*');
Route::get('/create-collect-book', 'Library\LibraryController@collectBookForm');
Route::post('/add/new/collect/book', 'Library\LibraryController@collectBookStore');
Route::get('/show/all/collect/book', 'Library\LibraryController@collectBooks');
Route::get('/book-collect-edit/{collect}', 'Library\LibraryController@collectBookEdit');
Route::delete('/collect-book-delete/{collect}', 'Library\LibraryController@collectBookDestroy');
Route::post('/collect-book-update/{collect}', 'Library\LibraryController@collectBookUpdate');
Route::get('/department-subjects/{department}', 'Library\LibraryController@departmentWishSubjects');


// StudentController

Route::get('/student-list', 'Student\StudentController@index')->where('any', '.*');
Route::get('/create-student', 'Student\StudentController@studentCreateForm');
Route::get('/xhr/show/all/class', 'Student\StudentController@showAllClasses');
