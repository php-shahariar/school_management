<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookCollectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_collects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('stu_id');
            $table->integer('subject_id');
            $table->integer('class_id');
            $table->string('group_name');
            $table->string('collect_date')->nullable();
            $table->string('return_date')->nullable();
            $table->boolean('status');
            //$table->string('photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_collects');
    }
}
