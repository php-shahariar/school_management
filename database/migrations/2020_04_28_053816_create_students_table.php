<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('stu_id')->unique();
            $table->string('name');
            $table->string('dob');
            $table->string('stu_email')->nullable();
            $table->string('stu_phone')->nullable();
            $table->integer('class_id');
            $table->string('group_name');
            $table->string('f_name');
            $table->string('m_name');
            $table->string('f_phone');
            $table->string('m_phone')->nullable();
            $table->string('f_nid');
            $table->string('m_nid');
            $table->text('present_address');
            $table->text('permanent_address');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
