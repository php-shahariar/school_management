<?php

namespace App\Http\Controllers\Library;

use App\BookCollect;
use App\Department;
use App\Http\Controllers\Controller;
use App\Library;
use App\Subject;
use Illuminate\Http\Request;

class LibraryController extends Controller
{
    public function index()
    {
        return view('backend.library.library');
    }

    public function create()
    {
        return view('backend.library.create');
    }

    public function subjects(Request $request)
    {
        return Subject::with('department')->get();
    }

    public function classes()
    {
        return Department::with('subjects')->get();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'class_id'   => 'required',
            'subject_id' => 'required',
            'group_name' => 'required',
            'status'     => 'required',
        ]);

        $book             = new Library();
        $book->subject_id = $request->subject_id;
        $book->class_id   = $request->class_id;
        $book->group_name = $request->group_name;
        $book->status     = $request->status;
        $book->created_by = auth()->user()->id;
        $book->save();
        return response()->json($book, 201);
    }

    public function books()
    {
        return Library::with('department', 'subjects')->get();
    }

    public function edit(Library $library)
    {
        return $library;
    }

    public function update(Request $request, Library $library)
    {
        $this->validate($request, [
            'class_id'   => 'required',
            'subject_id' => 'required',
            'group_name' => 'required',
            'status'     => 'required',
        ]);
        $library->subject_id = $request->subject_id;
        $library->class_id   = $request->class_id;
        $library->group_name = $request->group_name;
        $library->status     = $request->status;
        $library->updated_by = auth()->user()->id;
        $library->save();
        return response()->json($library, 200);
    }

    public function active(Request $request, Library $library)
    {
        $library->status = 1;
        $library->save();
        return response()->json($library, 200);
    }

    public function inactive(Request $request, Library $library)
    {
        $library->status = 0;
        $library->save();
        return response()->json($library, 200);
    }

    public function destroy(Library $library)
    {
        $library->delete();
        return response()->json('Book has been deleted');
    }

    public function trashList()
    {
        return view('backend.library.trash');
    }

    public function trash()
    {
        return Library::onlyTrashed()->with('department', 'subjects')->get();
    }

    public function restore($library)
    {
        $library = Library::onlyTrashed()->where('id', $library);
        $library->restore();
        return response()->json('Book has been restore');
    }

    public function libraryDelete($library)
    {
        $library = Library::onlyTrashed()->where('id', $library);
        $library->forceDelete();
        return response()->json('Book has been permanent delete');
    }

    public function showBookCollectForm()
    {
        return view('backend.library.collect');
    }

    public function collectBookStore(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
            'subject_id'    => 'required',
            'class_id'      => 'required',
            'group_name'    => 'required',
            'stu_id'        => 'required|max:8|min:2',
            'collect_date'  => 'required',
            'return_date'   => 'required',
            'status'        => 'required',
        ]);

        $bookCollect = new BookCollect();
        $bookCollect->name  = $request->name;
        $bookCollect->stu_id = $request->stu_id;
        $bookCollect->subject_id = $request->subject_id;
        $bookCollect->class_id = $request->class_id;
        $bookCollect->group_name = $request->group_name;
        $bookCollect->collect_date = $request->collect_date;
        $bookCollect->return_date = $request->return_date;
        $bookCollect->status = $request->status;
        $bookCollect->save();
        return response()->json($bookCollect, 201);
    }

    public function collectBooks()
    {
        return BookCollect::with('subjects', 'classes')->get();
    }

    public function collectBookEdit(BookCollect $collect)
    {
        return $collect;
    }

    public function collectBookDestroy(BookCollect $collect)
    {
        $collect->delete();
        return response()->json('Data delete has been successfully');
    }

    public function collectBookUpdate(Request $request, BookCollect $collect)
    {
        $this->validate($request, [
            'name'          => 'required',
            'subject_id'    => 'required',
            'class_id'      => 'required',
            'group_name'    => 'required',
            'stu_id'        => 'required|max:8|min:2',
            'collect_date'  => 'required',
            'return_date'   => 'required',
            'status'        => 'required',
        ]);

        $collect->name         = $request->name;
        $collect->stu_id       = $request->stu_id;
        $collect->subject_id   = $request->subject_id;
        $collect->class_id     = $request->class_id;
        $collect->group_name   = $request->group_name;
        $collect->collect_date = $request->collect_date;
        $collect->return_date  = $request->return_date;
        $collect->status       = $request->status;
        $collect->save();
        return response()->json($collect, 200);
    }

    public function departmentWishSubjects(Department $department)
    {
        return $department->subjects()->orderBy('id', 'desc')->get();
    }
}
