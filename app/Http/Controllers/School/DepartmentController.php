<?php

namespace App\Http\Controllers\School;

use App\Department;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
class DepartmentController extends Controller
{
    public function index()
    {
        return view('backend.department.class-list');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $addClass = new Department();
        $addClass->name = $request->name;
        $addClass->created_by = auth()->user()->id;
        $addClass->updated_by = auth()->user()->id;
        $addClass->save();
        return response()->json($addClass, 201);
    }

    public function classes(Request $request)
    {
        return Department::get();
    }

    public function edit(Department $department)
    {
        return $department;
    }

    public function update(Request $request, Department $department)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $department->name = $request->name;
        $department->created_by = auth()->user()->id;
        $department->updated_by = auth()->user()->id;
        $department->save();
        return response()->json($department, 200);
    }
    public function destroy(Department $department)
    {
        $department->delete();
        return response()->json('Class has been deleted');
    }
}
