<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\Controller;
use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function index()
    {
        return view('backend.subject.index');
    }

    public function create()
    {
        return view('backend.subject.create');
    }

    public function store(Request $request)
    {
        //return $request->all();
        $this->validate($request, [
            'name' => 'required',
            'class_id' => 'required',
            'group_id' => 'required',
        ]);

        $addNewSubject = new Subject();
        $addNewSubject->name     = $request->name;
        $addNewSubject->class_id = $request->class_id;
        $addNewSubject->group_id = $request->group_id;
        $addNewSubject->save();
        return response()->json($addNewSubject, 201);
    }

    public function subjects(Request $request)
    {
        return Subject::with('department')->get();
    }

    public function edit(Subject $subject)
    {
        return $subject;
    }

    public function update(Request $request, Subject $subject)
    {
        $this->validate($request, [
            'name' => 'required',
            'class_id' => 'required',
            'group_id' => 'required',
        ]);
        $subject->name     = $request->name;
        $subject->class_id = $request->class_id;
        $subject->group_id = $request->group_id;
        $subject->save();
        return response()->json($subject, 200);
    }

    public function destroy(Subject $subject)
    {
        $subject->delete();
        return response()->json('Subject has been deleted');
    }
}
