<?php

namespace App\Http\Controllers\Student;

use App\Department;
use App\Http\Controllers\Controller;
use App\Subject;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        return view('backend.students.index');
    }

    public function studentCreateForm()
    {
        return view('backend.students.create');
    }

    public function showAllClasses()
    {
        return Department::get();
    }

    public function showAllSubjects()
    {
        return Subject::get();
    }
}
