<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookCollect extends Model
{
    protected $guarded = [];

    public function classes()
    {
        return $this->belongsTo(Department::class, 'class_id', 'id');
    }

    public function subjects()
    {
        return $this->belongsTo(Subject::class, 'subject_id', 'id');
    }
}
