<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $guarded = [];

    public function subjects()
    {
        return $this->hasMany(Subject::class, 'class_id', 'id');
    }

    public function book()
    {
        return $this->belongsTo(Library::class, 'class_id', 'id');
    }

    public function bookCollect()
    {
        return $this->belongsTo(BookCollect::class, 'class_id', 'id');
    }
}
