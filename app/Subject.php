<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $guarded = [];

    /*
   |--------------------------------------------------------------------------
   | RELATIONS
   |--------------------------------------------------------------------------
   */
    public function department()
    {
        return $this->belongsTo(Department::class, 'class_id', 'id');
    }

    public function library()
    {
        return $this->belongsTo(Library::class, 'class_id', 'id');
    }

    public function collect()
    {
        return $this->belongsTo(BookCollect::class, 'subject_id', 'id');
    }
}
