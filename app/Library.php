<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Library extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    /*
   |--------------------------------------------------------------------------
   | RELATIONS
   |--------------------------------------------------------------------------
   */
    public function department()
    {
        return $this->belongsTo(Department::class, 'class_id', 'id');
    }

    public function subjects()
    {
        return $this->hasMany(Subject::class, 'id', 'subject_id');
    }
}
