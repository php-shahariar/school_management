<html>
<head>
    <title>School Login</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/') }}/style.css">
</head>
<body>
<div class="loginbox">
    <img src="{{ asset('/assets/') }}/avatar.png" class="avatar">
    <h1>Login Here</h1>
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <p>Username</p>
        <input type="text" name="email" placeholder="Enter Username">
        <p>Password</p>
        <input type="password" name="password" placeholder="Enter Password">
        <input type="submit" name="" value="Login">
    </form>
    @error('password')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
    @error('email')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>

</body>
</html>
