import HomeComponent from './components/academic/Home'
import ClassList from './components/academic/ClassList'
import ClassEdit from './components/academic/Edit'
import Subjects from './components/subject/Index'
import NewSubject from './components/subject/Create'
import Books from './components/library/Index'
import CreateBook from './components/library/Create'
import TrashList from './components/library/TrashList'
import CollectBook from './components/library/BookCollect'
import CreateBookCollect from './components/library/CreateBookCollect'
import StudentList from './components/student/Index'
import Create from './components/student/Create'
export const routes = [{
        path: '/home',
        component: HomeComponent
    },
    {
        path: '/class-list',
        component: ClassList,
    },
    {
        path: '/class-edit/:id',
        name: 'edit-class',
        component: ClassEdit,
    },
    {
        path: '/subjects',
        name: 'subjects-list',
        component: Subjects,
    },
    {
        path: '/create/subject',
        name: 'create-subject',
        component: NewSubject,
    },
    {
        path: '/subject-edit/:id',
        name: 'subject-edit',
        component: NewSubject,
    },
    {
        path: '/book-list',
        name: 'book-list',
        component: Books,
    },
    {
        path: '/create/book',
        name: 'create-book',
        component: CreateBook,
    },
    {
        path: '/trash/book/list',
        name: 'trash-book-list',
        component: TrashList,
    },
    {
        path: '/book-edit/:id',
        name: 'book-edit',
        component: CreateBook,
    },
    {
        path: '/collect-book',
        name: 'collect-book',
        component: CollectBook,
    },
    {
        path: '/create-collect-book',
        name: 'create-collect-book',
        component: CreateBookCollect,
    },
    {
        path: '/book-collect-edit/:id',
        name: 'book-collect-edit',
        component: CreateBookCollect,
    },

    {
        path: '/student-list',
        component: StudentList,
    },

    {
        path: '/create-student',
        name: 'create-student',
        component: Create,
    },
]