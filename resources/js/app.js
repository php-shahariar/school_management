/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// vue router
import VueRouter from 'vue-router'
Vue.use(VueRouter)



import {routes} from './routes'

 Vue.component('flash', require('./components/Flash.vue').default);
Vue.component('master', require('./components/academic/Master.vue').default)

import Paginate from 'vuejs-paginate'
Vue.component('paginate', Paginate)

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUserSecret)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

const router = new VueRouter({
    routes, // short for `routes: routes`
    mode:'history'
})

window.EventBus = new Vue();

const app = new Vue({
    el: '#app',
    router
});
